<?php
/**
 * Created by PhpStorm.
 * User: jefin
 * Date: 12/02/2018
 * Time: 10:45
 */
namespace ModernWays\Mvc;

interface IModel
{
    function getModelState();
    function setModelState(\ModernWays\Dialog\Model\INoticeBoard $modelState);

}