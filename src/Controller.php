<?php
/**
 * Created by PhpStorm.
 * User: jefin
 * Date: 10/02/2018
 * Time: 10:32
 */

namespace ModernWays\Mvc;

class Controller
{
    static public $namespace;
    static public $entity;
    static public $action;
    static public $id;

    /**
     * @param null $model
     * @param null $viewPath
     * @return \Closure
     */
    public static function view($model = null, $viewPath = null, $namespace = null)
    {
        if (!isset($namespace)) {
            $namespace = self::$namespace;
        }
        // copy file content into a string var
        $jsonFile = file_get_contents('composer.json');
        // convert the string to a json array
        $jfArray = json_decode($jsonFile, TRUE);
        if (isset($jfArray['autoload']['psr-4'][ $namespace . '\\'])) {
            $root = $jfArray['autoload']['psr-4'][$namespace . '\\'];
        } else {
            $root = '';
        }
        if (isset($viewPath)) {
            $path = "{$root}/View/{$viewPath}.php";
            // echo $path;
        } else {
            $trace = debug_backtrace();
            // echo '<pre>' . var_dump($trace) . '</pre>';
            $method = ucfirst($trace[1]["function"]);
            $class = $trace[1]["class"];
            $class = substr($class, strrpos($class, '\\') + 1);
            // $class = str_replace('Controller', '', $class);
            $path = "{$root}/View/{$class}/{$method}.php";
        }
         $view = function () use ($path, $model) {
            include($path);
        };
        // echo $path;
        return $view;
    }

    protected static function setFromUseCase($uc)
    {
        $route = explode('/', $uc);
        self::$namespace = $route[0];
        self::$entity = $route[1];
        self::$action = $route[2];
        self::$id = -1; // Standard parameter. Shouldn't appear in the database.
        if (isset($route[3])) {
            self::$id = $route[3];
        }
    }

    /**
     * Creates an instance of itself with the name passed in $entity
     * and invokes the method with the name passed in $action
     * @return bool|mixed
     */
    protected static function invokeAction()
    {
        // Functions, method calls, static class variables, and class constants inside {$} work since PHP 5.
        // However, the value accessed will be interpreted as the name of a variable in the scope in which the
        // string is defined. Using single curly braces ({}) will not work for accessing the return values of
        // functions or methods or the values of class constants or static class variables.
        $namespace = self::$namespace;
        $entity = self::$entity;
        $action = self::$action;
        $id = self::$id;
        $controllerName = "\\{$namespace}\\Controller\\{$entity}";
        $actionMethod = new \ReflectionMethod($controllerName, $action);
        if (!class_exists($controllerName, true)) {
            return false;
        } else {
            $reflection = new \ReflectionClass($controllerName);
            $controller = $reflection->newInstance();
            return $actionMethod->invokeArgs($controller, array($id));
        }
    }
}