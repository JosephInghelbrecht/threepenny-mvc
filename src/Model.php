<?php
/**
 * Created by PhpStorm.
 * User: jefin
 * Date: 12/02/2018
 * Time: 10:48
 */
namespace ModernWays\Mvc;

class Model implements IModel
{
    protected $modelState;

    /**
     * @return \ModernWays\Dialog\Model\INoticeBoard
     */
    public function getModelState()
    {
        return $this->modelState;
    }

    /**
     * @param null $modelState
     */
    public function setModelState(\ModernWays\Dialog\Model\INoticeBoard $modelState)
    {
        $this->modelState = $modelState;
    }
}